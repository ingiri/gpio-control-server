const led = (id, command) => {
    console.log(id, command)

    if (!command) command = id

    if (command == 'on') {
        console.log(`led id:${id} is now on`)
    }else {
        console.log(`led id:${id} is now off`)
    }
}

const button = (id, command) => {
    console.log(id, command)

    if (!command) command = id

    if (command == 'on') {
        console.log(`led id:${id} is now on`)
    }else {
        console.log(`led id:${id} is now off`)
    }
}

module.exports = {
    led,
    button
}