const path = require('path')
const express = require('express')
const twig = require('twig')


const gpio = require('./gpio')

gpio.led(1,'on')

const app = express()
const port = 3000


twig.cache(false)
app.engine('html', twig.__express);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');


app.use(express.static(path.join(__dirname, 'public')));


app.get('/', (req,res) => {
    res.render('index.html', { msg: 'hi'})
})
// button router
app.get('/led/on' , (req,res) => {
    gpio.button(1, 'on')
    res.send('on all leds')
    
})
app.get('/led/off' , (req,res) => {
    gpio.button(1, 'off')
    res.send('off all leds')
})
app.get('/led/:id/on', (req,res) => {
    let id = req.params.id
    console.log(id)
    gpio.button(id, 'on')
    res.send(`led ${id} now on`)
})
app.get('/led/:id/off', (req,res) => {
    let id = req.params.id
    console.log(id)
    gpio.button(id, 'off')
    res.send(`led ${id} now off`)
})
app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)

})